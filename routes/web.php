<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'middleware' => 'auth'
], function () {

    Route::get('/auth/get-user', function () {
        return Auth::user();
    });

    Route::get('unread-notifications', function () {
        return
            Auth::user()->unreadNotifications;
    });

    Route::get('/profile/{user?}', 'ProfileController@profile');

});

Route::group([
    'prefix' => 'friendship',
    'middleware' => 'auth'
], function () {

    Route::get('/friends/{user?}', 'FriendController@friends')->name('friends');
    Route::get('/pending', 'FriendController@pendingFriendships');
    Route::get('/waiting', 'FriendController@waitingFriendships');
    Route::get('/offer/{user}', 'FriendController@offerFriendship');
    Route::get('/accept/{user}', 'FriendController@acceptFriendship');
    Route::get('/cancel-pending/{user}', 'FriendController@cancelPendingFriendship');
    Route::get('/cancel-waiting/{user}', 'FriendController@cancelWaitingFriendship');
    Route::get('/delete/{user}', 'FriendController@deleteFriend');
    Route::get('/status/{user}', 'FriendController@status');

});

Route::group([
    'prefix' => 'dialog',
    'middleware' => 'auth'
], function () {

    Route::get('/all-dialogs', 'DialogController@allDialogs')->name('dialog.all');
    Route::get('/open-dialog/{dialog}', 'DialogController@openDialog')->name('dialog.open');
    Route::get('/writing-to-user/{user}', 'DialogController@writingToUser')->name('dialog.writing-to-user');
    Route::get('/get-messages/{dialog}', 'DialogController@getDialogMessages');
    Route::get('/get-users/{dialog}', 'DialogController@getDialogUsers');
    Route::post('/send-message/{dialog}', 'DialogController@sendMessage');

});

Auth::routes();

Route::get('/home', 'HomeController@index');
