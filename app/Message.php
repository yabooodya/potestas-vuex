<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];
    protected $with = ['user'];

    public function dialog() {
        return $this->belongsTo(Dialog::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
