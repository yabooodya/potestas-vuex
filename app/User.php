<?php

namespace App;

use App\Traits\Normals\DialogHelper;
use App\Traits\Normals\FriendHelper;
use App\Traits\Scopes\UserScope;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, FriendHelper, DialogHelper, UserScope;

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function dialogs() {
        return $this->belongsToMany(Dialog::class);
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function session() {
        return $this->hasOne(Session::class);
    }
}