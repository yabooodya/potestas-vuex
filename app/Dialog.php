<?php

namespace App;

use App\Traits\Scopes\DialogScope;
use Illuminate\Database\Eloquent\Model;

class Dialog extends Model
{
    use DialogScope;

    protected $guarded = [];

    public function latestMessage() {
        return $this->hasOne(Message::class)->latest();
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }
}
