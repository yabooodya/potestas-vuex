<?php

namespace App\Console\Commands;

use App\Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UserOnlineChangeStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:online';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change user online status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $online = Carbon::now()
            ->subMinutes(1)
            ->timestamp;

        $this->info('Online time was set: ' . $online);

        $lessSessions = Session::less($online)->pluck('user_id');
        $moreSessions = Session::more($online)->pluck('user_id');

        User::findMany($lessSessions)->each->update(['online' => true]);

        $this->info('Online status was set!');

        User::findMany($moreSessions)->each->update(['online' => false]);

        $this->info('Offline status was set!');
    }
}
