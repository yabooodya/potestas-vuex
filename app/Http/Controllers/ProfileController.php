<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile(User $user)
    {
        if (!$user->id) {
            $user = \Auth::user();
        }
        $user->status = $user->status($user->id);
        $user->isMe = $user->isMe();
        if (request()->ajax()) {
            return $user;
        }

        return view('profile', compact('user'));
    }
}
