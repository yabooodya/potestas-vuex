<?php

namespace App\Http\Controllers;

use App\Dialog;
use App\Events\NewDialogMessage;
use App\User;
use Auth;
use Illuminate\Http\Request;

class DialogController extends Controller
{
    public function allDialogs()
    {
        if (\Request::ajax()) {
            return Auth::user()->dialogs()->hasMessages()->with('latestMessage')->get();
        }
        return view('dialogs');
    }

    public function openDialog(Dialog $dialog)
    {
        if (\Request::ajax()) {
            return $dialog;
        }
        return view('dialog');
    }

    public function writingToUser(User $user)
    {
        if (\Request::ajax()) {
            return $user->writingToUser();
        }
        return view('dialog')->with('dialog', $user->writingToUser());
    }

    public function getDialogMessages(Dialog $dialog)
    {
        return $dialog->messages;
    }

    public function getDialogUsers(Dialog $dialog)
    {
        return $dialog->users;
    }

    public function sendMessage(Dialog $dialog, Request $request)
    {
        $message = Auth::user()->sendMessage($dialog, $request->all());
        event(new NewDialogMessage($message));
    }
}
