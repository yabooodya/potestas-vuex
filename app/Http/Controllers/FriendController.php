<?php

namespace App\Http\Controllers;

use App\User;
use Auth;

class FriendController extends Controller
{
    public function friends(User $user)
    {
        if (!$user->id) {
            $user = Auth::user();
        }

        $friends = $user->friends();

        if (\Request::ajax()) {
            return $friends;
        }
        return view('friends');
    }

    public function pendingFriendships()
    {
        return
            Auth::user()->pendingFriendships();
    }

    public function waitingFriendships()
    {
        return
            Auth::user()->waitingFriendships();
    }

    // Actions
    public function offerFriendship(User $user)
    {
        return
            $user->offerFriendship();
    }

    public function acceptFriendship(User $user)
    {
        return
            $user->acceptFriendship();
    }

    public function cancelPendingFriendship(User $user)
    {
        return
           $user->cancelPendingFriendship();
    }

    public function cancelWaitingFriendship(User $user)
    {
        return
            $user->cancelWaitingFriendship();
    }

    public function deleteFriend(User $user)
    {
        return
            $user->deleteFriend();
    }

    public function status(User $user)
    {
        return
            $user->status($user->id);
    }
}