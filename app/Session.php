<?php

namespace App;

use App\Traits\Scopes\SessionScope;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use SessionScope;

    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
