<?php

namespace App\Traits\Scopes;

use Auth;

trait DialogScope
{
    public function scopeExist($query, $user)
    {
        return
            $query
                ->whereHas('users', function ($query) {
                    $query->where('id', Auth::id());
                })
                ->whereHas('users', function ($query) use ($user) {
                    $query->where('id', $user);
                });
    }

    public function scopeHasMessages($query)
    {
        return $query->has('messages');
    }
}