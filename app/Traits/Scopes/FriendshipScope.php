<?php

namespace App\Traits\Scopes;

trait FriendshipScope
{
    public function scopeSender($query, $id) {
        return $query->where('sender', $id);
    }

    public function scopeRecipient($query, $id) {
        return $query->where('recipient', $id);
    }

    public function scopeSenderOrRecipient($query, $id) {
        return $query->where('sender', $id)->orWhere('recipient', $id);
    }

    public function scopeStatusFriend($query) {
        return $query->where('status', true);
    }

    public function scopeStatusNoFriend($query) {
        return $query->where('status', false);
    }
}