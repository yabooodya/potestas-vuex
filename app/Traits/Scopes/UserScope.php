<?php

namespace App\Traits\Scopes;

trait UserScope
{
    public function scopeOnline($query) {
        return $query->where('online', true);
    }

    public function scopeOffline($query) {
        return $query->where('online', false);
    }
}