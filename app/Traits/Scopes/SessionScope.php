<?php

namespace App\Traits\Scopes;


trait SessionScope
{
    public function scopeLess($query, $time) {
        return $query->where('last_activity', '>', $time);
    }

    public function scopeMore($query, $time){
        return $query->where('last_activity', '<', $time);
    }
}