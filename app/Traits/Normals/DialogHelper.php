<?php

namespace App\Traits\Normals;

use App\Dialog;
use App\Message;
use Auth;

trait DialogHelper
{
    public function writingToUser()
    {
        $dialog = Dialog::exist($this->id)->first();

        if ($dialog === null) {
            $dialog = Dialog::create(['name' => Auth::user()->name . ' | ' . $this->name]);
            $dialog->users()->sync([Auth::id(), $this->id]);
        }

        return $dialog;
    }

    public function sendMessage($dialog, $data)
    {
        $message = new Message($data);
        $message->user_id = $this->id;
        $message->dialog_id = $dialog->id;
        $message->save();

        return $message;
    }
}