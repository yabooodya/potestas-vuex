<?php

namespace App\Traits\Normals;

use App\Friendship;
use App\Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait FriendHelper
{
    // Models
    public function friends()
    {
        $friendships =
            Friendship::senderOrRecipient($this->id)
                ->statusFriend()
                ->get();

        $friends = User::findMany(($friendships->pluck('sender')->merge($friendships->pluck('recipient'))->flip()->except($this->id)->flip()));

        return $friends;
    }

    public function pendingFriendships()
    {
        $pendingFriendships =
            Friendship::sender($this->id)
                ->statusNoFriend()
                ->get();
        return $pendingFriendships;
    }

    public function waitingFriendships()
    {
        $friendships =
            Friendship::recipient($this->id)
                ->statusNoFriend()
                ->get();

        $waitingFriendships = User::findMany(($friendships->pluck('sender')->merge($friendships->pluck('recipient'))->flip()->except($this->id)->flip()));

        return $waitingFriendships;
    }

    // Actions
    public function offerFriendship()
    {
        if (!$this->hasRecord()) {
            $friendship =
                Friendship::create([
                    'sender' => Auth::id(),
                    'recipient' => $this->id
                ]);
//        $status = User::find($recipient)->status($this->id);
//        event(new StatusChanged($recipient, $status));
//        User::find($recipient)->notify(new NewFriendRequest(Auth::user()));

            return $friendship;
        }
        return null;
    }

    public function acceptFriendship()
    {
        if ($this->isWaiting()) {
            $friendship =
                Friendship::sender($this->id)
                    ->recipient(Auth::id())
                    ->statusNoFriend()
                    ->first();

            $friendship->update([
                'status' => true
            ]);
//            $status = User::find($user)->status($this->id);
//            event(new StatusChanged($user, $status));
//            User::find($user)->notify(new FriendRequestAccepted(Auth::user()));
            return $friendship;
        }
        return null;
    }

    public function cancelPendingFriendship()
    {
        if ($this->isPending()) {
            $friendship =
                Friendship::sender(Auth::id())
                    ->recipient($this->id)
                    ->statusNoFriend()
                    ->first();

            $friendship->delete();
//            $status = User::find($recipient)->status($this->id);
//            event(new StatusChanged($recipient, $status));
            return $friendship;
        }
        return null;
    }

    public function cancelWaitingFriendship()
    {
        if ($this->isWaiting()) {
            $friendship =
                Friendship::sender($this->id)
                    ->recipient(Auth::id())
                    ->statusNoFriend()
                    ->first();

            $friendship->delete();
//            $status = User::find($sender)->status($this->id);
//            event(new StatusChanged($sender, $status));
            return $friendship;
        }
        return null;
    }

    public function deleteFriend()
    {
        if ($this->isFriend()) {
            $friendship =
                Friendship::senderOrRecipient(Auth::id())
                    ->senderOrRecipient($this->id)
                    ->statusFriend()
                    ->first();

            $friendship->update([
                'sender' => $this->id,
                'recipient' => Auth::id(),
                'status' => false
            ]);
//            $status = User::find($friend)->status($this->id);
//            event(new StatusChanged($friend, $status));
            return $friendship;
        }
        return null;
    }

    public function isPending()
    {
        if (!$this->isMe()) {
            return
                Friendship::sender(Auth::id())
                    ->recipient($this->id)
                    ->statusNoFriend()
                    ->first() ? 'pending' : null;
        }
        return null;
    }

    public function isWaiting()
    {
        if (!$this->isMe()) {
            return
                Friendship::sender($this->id)
                    ->recipient(Auth::id())
                    ->statusNoFriend()
                    ->first() ? 'waiting' : null;
        }
        return null;
    }

    public function isFriend()
    {
        if (!$this->isMe()) {
            return
                Friendship::senderOrRecipient(Auth::id())
                    ->senderOrRecipient($this->id)
                    ->statusFriend()
                    ->first() ? 'friend' : null;
        }
        return null;
    }

    public function hasRecord()
    {
        return
            $this->isMe() || Friendship::senderOrRecipient(Auth::id())
                ->senderOrRecipient($this->id)
                ->first();
    }

    public function isMe()
    {
        return $this->id === Auth::id();
    }

    public function status()
    {
        if ($this->hasRecord()) {
            return
                $this->isPending() ??
                $this->isWaiting() ??
                $this->isFriend() ??
                0;
        }
        return 0;
    }
}