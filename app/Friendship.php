<?php

namespace App;

use App\Traits\Scopes\FriendshipScope;
use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    use FriendshipScope;

    protected $guarded = [];
}