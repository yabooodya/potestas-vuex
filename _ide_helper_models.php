<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Dialog
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Dialog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Dialog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Dialog whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Dialog whereUpdatedAt($value)
 */
	class Dialog extends \Eloquent {}
}

namespace App{
/**
 * App\Friendship
 *
 * @property int $id
 * @property int $sender
 * @property int $recipient
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship recipient($id)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship sender($id)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship senderOrRecipient($id)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship statusFriend()
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship statusNoFriend()
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship whereRecipient($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship whereSender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Friendship whereUpdatedAt($value)
 */
	class Friendship extends \Eloquent {}
}

namespace App{
/**
 * App\Message
 *
 * @property int $id
 * @property int $dialog_id
 * @property int $user_id
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Dialog $dialog
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereDialogId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUserId($value)
 */
	class Message extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dialog[] $dialogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

