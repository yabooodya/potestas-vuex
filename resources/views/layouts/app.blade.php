<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/yabodya.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">

                            <!— Collapsed Hamburger —>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#app-navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!— Branding Image —>
                            <a class="navbar-brand" href="{{ url('/') }}">
                                {{ config('app.name', 'Laravel') }}
                            </a>
                        </div>

                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            <!— Left Side Of Navbar —>
                            <ul class="nav navbar-nav">
                                @if(Auth::check())
                                    <unread-notifications></unread-notifications>
                                    <li>
                                        <a href="{{ route('dialog.all') }}">
                                            <i class="fa fa-comments-o fa-2x"></i>
                                        </a>
                                    </li>
                                @endif
                            </ul>

                            <!— Right Side Of Navbar —>
                            <ul class="nav navbar-nav navbar-right">
                                <!— Authentication Links —>
                                @if (Auth::guest())
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    Выйти
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            @if(Auth::check())
            <div class="col-md-2">
                <sidebar></sidebar>
            </div>
            @endif
            <div class="col-md-10">
                @if(Auth::check())
                    <transition enter-active-class="animated fadeInDown">
                        <router-view></router-view>
                    </transition>
                @else
                    @yield('content')
                @endif
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
@if(Auth::check())
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
@endif
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
