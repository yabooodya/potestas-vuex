import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: '/profile/:id?',
        name: 'profile',
        component: require('./components/Profile/Profile.vue'),
        props: true
    },
    {
        path: '/dialog/all-dialogs',
        name: 'dialogs',
        component: require('./components/Dialog/DialogList.vue')
    },
    {
        path: '/dialog/open-dialog/:id',
        name: 'dialog',
        component: require('./components/Dialog/DialogOpen.vue'),
        props: true
    },
    {
        path: '/friendship/friends',
        name: 'friends',
        component: require('./components/Friend/FriendList.vue')
    }
];

export const router = new VueRouter({
    mode: 'history',
    routes
});