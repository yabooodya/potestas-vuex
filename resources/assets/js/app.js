require('./bootstrap');

// Vue.component('dialog-list', require('./components/Dialog/DialogList.vue'));
Vue.component('dialog-list-item', require('./components/Dialog/DialogListItem.vue'));
Vue.component('dialog-open', require('./components/Dialog/DialogOpen.vue'));

// Vue.component('friend-list', require('./components/Friend/FriendList.vue'));

Vue.component('notification', require('./components/Other/Notification.vue'));
Vue.component('unread-notifications', require('./components/Other/UnreadNotifications.vue'));

Vue.component('sidebar', require('./components/Navigation/Sidebar.vue'));
Vue.component('init-app-data', require('./components/Init.vue'));

import { store } from './store';
import { router } from './vue-router';

const app = new Vue({
    router,
    store
}).$mount('#app');
