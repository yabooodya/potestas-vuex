import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        user: {},
        notifications: []
    },
    getters: {
        getUser(state) {
            return state.user;
        },
        allNotifications(state) {
            return state.notifications;
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        },
        addNotification(state, notification) {
            state.notifications.push(notification);
        }
    },
    actions: {}
});